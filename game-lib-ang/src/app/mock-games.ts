import { Game } from './Game';
export const GAMES: Game[] = [
    {
        id: 1,
        name: "Doom Ethernal",
        price: 200,
        currency: "UAH",
        description: `Hell’s armies have invaded Earth. Become the Slayer in an epic single-player campaign to conquer demons across dimensions and stop the final destruction of humanity.
        The only thing they fear... is you.`,
        purchased: false,
        tag: "action"
    },

      {
        id: 2,
        name: "Doom Ethernal2",
        price: 200,
        currency: "UAH",
        description: `Hell’s armies have invaded Earth. Become the Slayer in an epic single-player campaign to conquer demons across dimensions and stop the final destruction of humanity.
        The only thing they fear... is you.`,
        purchased: false,
        tag: "action"
    },
      
        {
        id: 3,
        name: "Doom Ethernal3",
        price: 200,
        currency: "UAH",
        description: `Hell’s armies have invaded Earth. Become the Slayer in an epic single-player campaign to conquer demons across dimensions and stop the final destruction of humanity.
        The only thing they fear... is you.`,
        purchased: false,
        tag: "action"
    },
        {
        id: 4,
        name: "Doom Ethernal4",
        price: 200,
        currency: "UAH",
        description: `Hell’s armies have invaded Earth. Become the Slayer in an epic single-player campaign to conquer demons across dimensions and stop the final destruction of humanity.
        The only thing they fear... is you.`,
        purchased: false,
        tag: "action"
    },
        {
        id: 5,
        name: "Doom Ethernal5",
        price: 200,
        currency: "UAH",
        description: `Hell’s armies have invaded Earth. Become the Slayer in an epic single-player campaign to conquer demons across dimensions and stop the final destruction of humanity.
        The only thing they fear... is you.`,
        purchased: true,
        tag: "adventure"
    },
        {
        id: 6,
        name: "Doom Ethernal6",
        price: 200,
        currency: "UAH",
        description: `Hell’s armies have invaded Earth. Become the Slayer in an epic single-player campaign to conquer demons across dimensions and stop the final destruction of humanity.
        The only thing they fear... is you.`,
        purchased: false,
        tag: "indie"
    }
              
]